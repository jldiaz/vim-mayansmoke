vim mayansmoke color scheme
===

A copy in bitbucket of the [Mayan Smoke][1] color scheme, which I did to ease its installation in different machines using [pathogen][2].

Screenshot
--

(The link points to one of the screenshots provided by the author Jeet Sukumaran):

![Screenshot](http://jeetworks.org/sites/default/files/images/screenshots/mayansmoke-python1.png)

Credits
--

Created by Jeet Sukumaran

[1]: http://jeetworks.org/mayansmoke
[2]: https://github.com/tpope/vim-pathogen

